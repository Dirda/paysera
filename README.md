# Instalation
1. nodejs
2. npm install -g @angular/cli
3. npm install

# Paysera

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Instructions

File is automaticaly read on startup from 'assets' folder with a name of 'input.json'
After clicking 'Work!' button we can see list with result of simple logics
Not implemented:
    1. Read file from input
    2. Write to file
    3. Different currency
    4. Tests
    5. Security for invalid data