import { PayseraPage } from './app.po';

describe('paysera App', () => {
  let page: PayseraPage;

  beforeEach(() => {
    page = new PayseraPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
