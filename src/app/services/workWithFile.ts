import { Component, Input } from '@angular/core';
import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Operation } from '../models/operation';

@Injectable()
export class WorkWithFileService {

    _operations: Operation[] = [];

    constructor(private http: Http) {
    }

    getDataFromFile(): Observable<any> {
        return this.http.get('assets/input.json')
            .map((res:any) => res.json());
    }

    readFromFile() {
        this.getDataFromFile().subscribe((data: any) => {
            this._operations = data;
            console.log('data', data);
        },
        err => {
            console.error(err);
        });
    }
}