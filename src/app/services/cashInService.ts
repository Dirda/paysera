import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { CashInResponse } from '../models/cashInResponse';

@Injectable()
export class CashInServiceService {

    config: CashInResponse;

    constructor(
        private _ServerService: ServerService
    ) { 
        this._ServerService.getCashIn().subscribe(
            (response) => this.config = response,
            (error) => console.error(error)
        );
    }

    getTax(total: number): number {
        let result: number;
        result = total / 100 * this.config.percents;
        if (result > this.config.max.amount) {
            result = this.config.max.amount;
        }
        return Number(result.toFixed(2));
    }

    
}