import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServerService {

    url: string = 'http://private-anon-565e45a30f-uzduotis.apiary-mock.com';
    headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: Http) { }

    getCashIn() {
        return this.http.get(this.url + '/config/cash-in').map(
            (response: Response) => {
                return response.json();
            }
        ).catch(
            (error: Response) => {
                return Observable.throw('Something went wrong');
            }
        );
    }

    getCashOutNatural() {
        return this.http.get(this.url + '/config/cash-out/natural').map(
            (response: Response) => {
                return response.json();
            }
        ).catch(
            (error: Response) => {
                return Observable.throw('Something went wrong');
            }
        );
    }

    getCashOutJuridical() {
        return this.http.get(this.url + '/config/cash-out/juridical').map(
            (response: Response) => {
                return response.json();
            }
        ).catch(
            (error: Response) => {
                return Observable.throw('Something went wrong');
            }
        );
    }

    getRates() {
        return this.http.get(this.url + '/rates').map(
            (response: Response) => {
                return response.json();
            }
        ).catch(
            (error: Response) => {
                return Observable.throw('Something went wrong');
            }
        );
    }
    
}