import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { CashInResponse } from '../models/cashInResponse';
import { NaturalResponse } from '../models/naturalResponse';
import { NaturalUserAction } from '../models/naturalUserAction';
import { Operation } from '../models/operation';
import * as moment from 'moment';

@Injectable()
export class CashOutNaturalService {

    config: NaturalResponse;
    userAction: NaturalUserAction[] = [];

    constructor(
        private _ServerService: ServerService
    ) { 
        this._ServerService.getCashOutNatural().subscribe(
            (response) => this.config = response,
            (error) => console.error(error)
        );
    }

    resetCashout() {
        this.userAction = [];
    }

    getTax(operation: Operation): number {
        let currentUserActions: NaturalUserAction = this.userAction.filter((item) => {
            return (item.userId === operation.user_id) && 
                (item.cashout.year === new Date(operation.date).getFullYear()) &&
                (item.cashout.week === this.getYearsWeek(operation.date));
        })[0];

        if (currentUserActions) {
            currentUserActions.cashout.sum += operation.operation.amount;
            let result = this.getUserLimitOverused(currentUserActions, operation, false);
            return Number(result / 100 * this.config.percents);
        } else {
            let newUserAction: NaturalUserAction = {
                userId: operation.user_id,
                cashout: {
                    week: this.getYearsWeek(operation.date),
                    year: new Date(operation.date).getFullYear(),
                    sum: operation.operation.amount
                }
            };
            this.userAction.push(newUserAction);
            let result = this.getUserLimitOverused(newUserAction, operation, true);
            return Number(result / 100 * this.config.percents);
        }
    }

    private getUserLimitOverused(userAction: NaturalUserAction, operation: Operation, newOperation: boolean): number {
        let result: number = 0;
        if ((userAction.cashout.sum >= this.config.week_limit.amount) && newOperation) {
            // first time operation and more
            result = userAction.cashout.sum;
            return result - this.config.week_limit.amount;
        } else if ((userAction.cashout.sum >= this.config.week_limit.amount) && !newOperation) {
            // not first time operation and more
            if ((userAction.cashout.sum - operation.operation.amount) >= this.config.week_limit.amount) {
                // limit has been reached before
                result = operation.operation.amount;
            } else {
                // limit reached on this operation
                result = this.config.week_limit.amount - userAction.cashout.sum + operation.operation.amount;
            }
        } else if ((userAction.cashout.sum + operation.operation.amount) >= this.config.week_limit.amount) {
            // limit has not beed reached
            result = userAction.cashout.sum + operation.operation.amount - this.config.week_limit.amount;
        }
        return result
    }

    private getYearsWeek(inputDate) {
      let date: moment.Moment = moment(inputDate);
      date.hours(0).minutes(0).seconds(0).milliseconds(0);
      // Thursday in current week decides the year.
      date.date(date.date() + 3 - (date.day() + 6) % 7);
      // January 4 is always in week 1.
      var week1 = moment([date.year(), 0, 4]);
      // Adjust to Thursday in week 1 and count number of weeks from date to week1.
      return 1 + Math.round(((date.unix() * 1000 - week1.unix() * 1000) / 86400000 - 3 + (week1.day() + 6) % 7) / 7);
    }

    
}