import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { CashInResponse } from '../models/cashInResponse';
import { JuridicalResponse } from '../models/juridicalResponse';

@Injectable()
export class CashOutJuridicalService {

    config: JuridicalResponse;

    constructor(
        private _ServerService: ServerService
    ) { 
        this._ServerService.getCashOutJuridical().subscribe(
            (response) => this.config = response,
            (error) => console.error(error)
        );
    }

    getTax(total: number): number {
        let result: number;
        result = total / 100 * this.config.percents;
        if (result < this.config.min.amount) {
            result = this.config.min.amount;
        }
        return Number(result.toFixed(2));
    }

    
}