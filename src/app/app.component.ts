import { Component } from '@angular/core';
import { ServerService } from './services/server.service';
import { Response } from '@angular/http';
import { Operation } from './models/operation';
import { CashInServiceService } from './services/cashInService';
import { CashOutJuridicalService } from './services/cashOutJuridical';
import { CashOutNaturalService } from './services/cashOutNatural';
import { WorkWithFileService } from './services/workWithFile';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Paysera demo';
  _operations: Operation[] = [];
  result: number[] = [];

  constructor(
    private serverService: ServerService,
    private cashInServiceService: CashInServiceService,
    private cashOutJuridicalService: CashOutJuridicalService,
    private cashOutNatural: CashOutNaturalService,
    private workWithFileService: WorkWithFileService) {
        this.workWithFileService.getDataFromFile().subscribe((data: any) => {
            this._operations = data;
            console.log('data', data);
        },
        error => {
            console.error(error);
        })
    }

  work() {
    this.result = [];  
    for (let i = 0; i < this._operations.length; i++) {
        if (this._operations[i].type === 'cash_in') {
            this.getCashIn(this._operations[i].operation.amount);
        } else if (this._operations[i].type === 'cash_out') {
            if (this._operations[i].user_type === 'natural') {
                this.getCashOutNatural(this._operations[i]);
            } else if (this._operations[i].user_type === 'juridical') {
                this.getCashOutJuridical(this._operations[i].operation.amount);
            }
        }
    }
  }  

  getCashIn(amount: number) {
      this.result.push(this.cashInServiceService.getTax(amount));
  }

  getCashOutNatural(operation: Operation) {
      this.result.push(this.cashOutNatural.getTax(operation));
  }

  getCashOutJuridical(amount: number) {
      this.result.push(this.cashOutJuridicalService.getTax(amount));
  }

  getRates() {
      this.serverService.getRates().subscribe(
          (response) => console.log(response),
          (error) => console.log(error)
      );
  }
    
}
