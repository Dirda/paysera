export class NaturalUserAction {
    userId: number;
    cashout: {
        week: number;
        year: number;
        sum: number;
    }
}