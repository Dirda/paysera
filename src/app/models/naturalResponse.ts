export class NaturalResponse {
    percents: number;
    week_limit: {
        amount: number;
        currency: string;
    }
}