export class CashInResponse {
    percents: number;
    max: {
        amount: number;
        currency: string;
    }
}