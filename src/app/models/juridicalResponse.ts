export class JuridicalResponse {
    percents: number;
    min: {
        amount: number;
        currency: string;
    }
}