import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ServerService } from './services/server.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CashInServiceService } from './services/cashInService';
import { CashOutJuridicalService } from './services/cashOutJuridical';
import { CashOutNaturalService } from './services/cashOutNatural';
import { WorkWithFileService } from './services/workWithFile';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    ServerService,
    WorkWithFileService,
    CashInServiceService,
    CashOutJuridicalService,
    CashOutNaturalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
